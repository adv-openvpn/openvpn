FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > openvpn.log'

COPY openvpn .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' openvpn
RUN bash ./docker.sh

RUN rm --force --recursive openvpn
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD openvpn
